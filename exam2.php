<?php
require 'header.php';
require 'conn.php';
?>
<div class="container">
    <?php
    $sql = "SELECT std_id, class_id, exam_id,english ,nepali, mathematics, science, socialstudies, EPH, Computerscience, Optionalmaths FROM marks where exam_id=2";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    
    echo "<table>
    <thead>
    <tr>
    <th>Student_id</th>
        <th>Class_id</th>
        <th>Exam_id</th>
        <th>English</th>
        <th>Nepali</th>
        <th>Mathematics</th>
        <th>Science</th>
        <th>Social Studies</th>
        <th>EPH</th>
        <th>Computer Science</th>
        <th>Optional Maths</th>
        
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["std_id"]."</td>";
        echo "<td>". $row["class_id"]."</td>";
        echo "<td>". $row["exam_id"]."</td>";
        echo "<td>". $row["english"]."</td>";
        echo "<td>". $row["nepali"]."</td>";
        echo "<td>". $row["mathematics"]."</td>";
        echo "<td>". $row["science"]."</td>";
        echo "<td>". $row["socialstudies"]."</td>";
        echo "<td>". $row["EPH"]."</td>";
        echo "<td>". $row["Computerscience"]."</td>";
        echo "<td>". $row["Optionalmaths"]."</td>";
        

        echo '<td><a href="edit.php?id=' . $row['subject_id'] . '">Edit</a> <a href="deletemark.php?id=' . $row['std_id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);


?>
<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>
<a href="marks.php" button type="button" class="btn btn-primary">Back</button>