<?php
require 'header.php';
require 'conn.php';
?>
<div class="container">
    <?php
$sql = "SELECT exam_id, exam_name FROM examtype";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    echo "<table>
    <thead>
    <tr>
    <th>exam_id</th>
    <th>exam_name</th>
    <th>Action</th>
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["exam_id"]."</td>";
        echo "<td>". $row["exam_name"]."</td>";
        echo '<td><a href="edit.php?id=' . $row['exam_id'] . '">Edit</a> <a href="deleteexamtype.php?id=' . $row['exam_id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);
?>

<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>
<a href="examtype.php" button type="button" class="btn btn-primary">Back</button>


</div>