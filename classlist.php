<?php
include 'header.php';
include 'conn.php';
?>
<div class="container">
    <?php
$sql = "SELECT class_id, class_name FROM class";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    echo "<table>
    <thead>
    <tr>
    <th>class_id</th>
    <th>class_name</th>
    <th>Action</th>
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["class_id"]."</td>";
        echo "<td>". $row["class_name"]."</td>";
        echo '<td><a href="viewstudent.php">View</a> <a href="deleteclass.php?id=' . $row['class_id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);
?>
<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>

<a href="class.php" button type="button" class="btn btn-primary">Back</button>

</div>