<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href= "css/style.css" rel ="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
<div class="main-container">
<header>
<div class="logo"><h5><img src="logo.jpg" height="130px" width="130px">Pragati Prabhat Academy</h5></div>

</header>


<nav class="nav">
  <ul>
    <li>
      <a href="student.php">Student</a>
    </li>
    <li>
      <a href="class.php">Class</a>
    </li>
    <li>
      <a href="subject.php">Subject</a>
    </li>
    <li>
      <a href="examtype.php">Exam Type</a>
    </li>

    <li>
    <a href="marks.php">Marks</a>
    </li>
    <li>
      <a href="find-result.php">Generate Report</a>
    </li>
  </ul>
</nav>

</div>


</body>
</html>