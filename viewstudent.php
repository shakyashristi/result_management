<?php
require 'header.php';
require 'conn.php';
?>
<div class="container">
    <?php
$sql = "SELECT std_id, name, roll_number, address, dateofbirth, class_id FROM student where class_id=1";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    
    echo "<table>
    <thead>
    <tr>
    <th>student_id</th>
    <th>Name</th>
    <th>Address</th>
    <th>Roll_number</th>
    <th>dateofbirth</th>
    <th>class_id</th>
    <th>Action</th>
    </tr>
    </thead>";
    echo "<tbody>";
    while($row = mysqli_fetch_assoc($result)) {
        
        echo"<tr>";
        echo "<td>". $row["std_id"]."</td>";
        echo "<td>". $row["name"]."</td>";
        echo "<td>". $row["address"]."</td>";
        echo "<td>". $row["roll_number"]."</td>";
        echo "<td>". $row["dateofbirth"]."</td>";
        echo "<td>". $row["class_id"]."</td>";
        echo '<td><a href="edit.php?id=' . $row['std_id'] . '">Edit</a> <a href="delete.php?id=' . $row['std_id'] . '"onclick="return confirmation()">Delete</a></td>';


        echo"</tr>";
       
       
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}

mysqli_close($conn);
?>
<script type="text/javascript">
    function confirmation() {
      return confirm('Are you sure you want to delete this?');
    }
</script>

<a href="class.php" button type="button" class="btn btn-primary">Back</button>

</div>