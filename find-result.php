<?php
require 'header.php';
require 'conn.php';
?>

<div class="container">
<form action="result.php" method="post">
<div class="form-group">
<label for="std_id">Student Id</label>
<input type="number" class="form-control" id="std_id" placeholder="Enter Student Id" autocomplete="off" name="std_id">
</div>
<div class="form-group">
<label for="name">Student Name</label>
<input type="text" class="form-control" id="name" placeholder="Enter Student name" autocomplete="off" name="name">
</div>
<div class="form-group">
<label for="class_id">Class</label>
<input type="number" class="form-control" id="class_id" placeholder="Enter Class Id" autocomplete="off" name="class_id">
</div>
<div class="form-group">
<label for="exam_id">Exam Id</label>
<input type="number" class="form-control" id="exam_id" placeholder="Enter Exam Id" autocomplete="off" name="exam_id">
</div>
<button type="submit" class="btn btn-success btn-labeled pull-right">Search<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
<a href="index.php">Back to Home</a>
</form>
</div>




